
import string
import pandas as pd
import hashlib
import requests
import time as t
import sqlite3

class BaseDatos():
    def __init__(self):
        self.conexion = sqlite3.connect('time.db')

    def crear_tabla(self):
        try:
            self.conexion.execute("create table IF NOT EXISTS tiempo(id integer primary key autoincrement, Max float, Min float, Promedio float);")
        except sqlite3.OperationalError:
            print("tabla ya existente")    
    
    def save(self, max,min,promedio):
        try:
            self.crear_tabla()
            self.conexion.execute("insert into tiempo('Max','Min','Promedio') values ({},{},{})".format(max,min,promedio))
            for result in self.conexion.execute("select * from tiempo;"):
                print(result)
            self.conexion.close()
        except Exception as e:
            print("ocurrio un error al guardar los datos", e)    


class Ejercicio(BaseDatos):
    def __init__(self) -> string:
        self.url = 'https://restcountries.com/v3.1/name/angola'
        super().__init__()

    def set_dataframe(self):
        inicio = t.time()
        datos = requests.get(self.url)
        json_datos = datos.json()
        dframe=pd.DataFrame(self.get_datos(json_datos, inicio), columns=["Region", "City Name", "Langueje", "time"])
        print(dframe)
        super().save(dframe['time'].max(), dframe['time'].min(), dframe['time'].mean())
        dframe.to_json('json/data.json', orient='records')

    def get_datos(self, json_data, inicio_tiempo):
        tabla_list = list()
        name, languages, region = None,None,None
        for i in json_data:
            name = i['name']['common']
            languages = [hashlib.sha1(bytes("{}".format(v), encoding='utf8')).hexdigest() for k,v in i["languages"].items()]
            region = i['region']
            break
        tabla_list.extend([[region,name,languages[0], t.time()-inicio_tiempo]]) 
        return tabla_list


if __name__ == '__main__':
    Ejercicio().set_dataframe()

